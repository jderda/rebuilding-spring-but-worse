```
     .->    (`-').->  (`-')  _  (`-')       ,------.  
 (`(`-')/`) (OO )__   (OO ).-/  ( OO).->   /  .--.  ' 
,-`( OO).',,--. ,'-'  / ,---.   /    '._   |  |  |  | 
|  |\  |  ||  | |  |  | \ /`.\  |'--...__) `--'__.  | 
|  | '.|  ||  `-'  |  '-'|_.' | `--.  .--'    |   .'  
|  |.'.|  ||  .-.  | (|  .-.  |    |  |       |___|   
|   ,'.   ||  | |  |  |  | |  |    |  |       .---.   
`--'   '--'`--' `--'  `--' `--'    `--'       `---'   

Spring:
* Spring Core <-- our focus (sort of)
* Spring Data
* Spring Boot
* Spring Web <-- because why not
* Spring Test
* Spring CoffeServingRobots











```

```

  .---.    .-./`) ,---.   .--.  .-_'''-.       ,-----.     
  | ,_|    \ .-.')|    \  |  | '_( )_   \    .'  .-,  '.   
,-./  )    / `-' \|  ,  \ |  ||(_ o _)|  '  / ,-.|  \ _ \  
\  '_ '`)   `-'`"`|  |\_ \|  |. (_,_)/___| ;  \  '_ /  | : 
 > (_)  )   .---. |  _( )_\  ||  |  .-----.|  _`,/ \ _/  | 
(  .  .-'   |   | | (_ o _)  |'  \  '-   .': (  '\_/ \   ; 
 `-'`-'|___ |   | |  (_,_)\  | \  `-'`   |  \ `"/  \  ) /  
  |        \|   | |  |    |  |  \        /   '. \_/``".'   
  `--------`'---' '--'    '--'   `'-...-'      '-----'     
                                                           

* Dependency Injection / Inversion of Control
* Container
* Context
* Injector
* Injection point
* Wiring / autowiring
* Bean / Component










```

```

                                         _.--,-```-.    
        ,--,                            /    /      '.  
      ,--.'|                           /  ../         ; 
   ,--,  | :                           \  ``\  .``-    '
,---.'|  : '    ,---.            .---.  \ ___\/    \   :
|   | : _' |   '   ,'\          /. ./|        \    :   |
:   : |.'  |  /   /   |      .-'-. ' |        |    ;  . 
|   ' '  ; : .   ; ,. :     /___/ \: |       ;   ;   :  
'   |  .'. | '   | |: :  .-'.. '   ' .      /   :   :   
|   | :  | ' '   | .; : /___/ \:     '      `---'.  |   
'   : |  : ; |   :    | .   \  ' .\          `--..`;    
|   | '  ,/   \   \  /   \   \   ' \ |     .--,_        
;   : ;--'     `----'     \   \  |--"      |    |`.     
|   ,/                     \   \ |         `-- -`, ;    
'---'                       '---"            '---`"     
                                                        

* As simple as possible (we have 45 minutes)
* We'll glimpse over most of boilerplate
* Using Java (the better of languages) and Eclipse (the better of IDEs)
* Won't create RFC and use it as base of Personio web stack (please)
* Disclaimer: opinionated and not best practices by a long stretch










```



```

 /$$      /$$ /$$                  /$$$$ 
| $$  /$ | $$| $$                 /$$  $$
| $$ /$$$| $$| $$$$$$$  /$$   /$$|__/\ $$
| $$/$$ $$ $$| $$__  $$| $$  | $$    /$$/
| $$$$_  $$$$| $$  \ $$| $$  | $$   /$$/ 
| $$$/ \  $$$| $$  | $$| $$  | $$  |__/  
| $$/   \  $$| $$  | $$|  $$$$$$$   /$$  
|__/     \__/|__/  |__/ \____  $$  |__/  
                        /$$  | $$        
                       |  $$$$$$/        
                        \______/         

* because it's fun ... 
* ... and let's us understand better why it does what it does




















```



```











     ___  _______  __   __  _______    ______    _______  _______  _______  _______ 
    |   ||   _   ||  | |  ||   _   |  |    _ |  |       ||       ||   _   ||       |
    |   ||  |_|  ||  |_|  ||  |_|  |  |   | ||  |    ___||       ||  |_|  ||    _  |
    |   ||       ||       ||       |  |   |_||_ |   |___ |       ||       ||   |_| |
 ___|   ||       ||       ||       |  |    __  ||    ___||      _||       ||    ___|
|       ||   _   | |     | |   _   |  |   |  | ||   |___ |     |_ |   _   ||   |    
|_______||__| |__|  |___|  |__| |__|  |___|  |_||_______||_______||__| |__||___|    











```



```











  ____       U  ___ u   _   _      _    _____         ____       U  ___ u                     _____     _    
 |  _"\       \/"_ \/  | \ |"|    |"|  |_ " _|       |  _"\       \/"_ \/          ___       |_ " _|  U|"|u  
/| | | |      | | | | <|  \| |>   |_|    | |        /| | | |      | | | |         |_"_|        | |    \| |/  
U| |_| |\ .-,_| |_| | U| |\  |u         /| |\       U| |_| |\ .-,_| |_| |          | |        /| |\    |_|   
 |____/ u  \_)-\___/   |_| \_|         u |_|U        |____/ u  \_)-\___/         U/| |\u     u |_|U    (_)   
  |||_          \\     ||   \\,-.      _// \\_        |||_          \\        .-,_|___|_,-.  _// \\_   |||_  
 (__)_)        (__)    (_")  (_/      (__) (__)      (__)_)        (__)        \_)-' '-(_/  (__) (__) (__)_) 


( Unless you know what you're doing and there is no other way )












```


```












   _               _       (")                        _                       _      _     
  | |      ___    | |_      \|     ___      o O O  __| |    ___      o O O   (_)    | |_   
  | |__   / -_)   |  _|           (_-<     o      / _` |   / _ \    o        | |    |  _|  
  |____|  \___|   _\__|   _____   /__/_   TS__[O] \__,_|   \___/   TS__[O]  _|_|_   _\__|  
_|"""""|_|"""""|_|"""""|_|     |_|"""""| {======|_|"""""|_|"""""| {======|_|"""""|_|"""""| 
"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'./o--000'"`-0-0-'"`-0-0-'./o--000'"`-0-0-'"`-0-0-' 













```

  
  
```


        ,----,                                                                                          
      ,/   .`|                                                                                          
    ,`   .'  :                ,-.                                                                       
  ;    ;     /            ,--/ /|                                                                       
.'___,/    ,'           ,--. :/ |                                .---.                                  
|    :     |            :  : ' /                                /. ./|                       .--.--.    
;    |.';  ;  ,--.--.   |  '  /      ,---.     ,--.--.       .-'-. ' |  ,--.--.        .--, /  /    '   
`----'  |  | /       \  '  |  :     /     \   /       \     /___/ \: | /       \     /_ ./||  :  /`./   
    '   :  ;.--.  .-. | |  |   \   /    /  | .--.  .-. | .-'.. '   ' ..--.  .-. | , ' , ' :|  :  ;_     
    |   |  ' \__\/: . . '  : |. \ .    ' / |  \__\/: . ./___/ \:     ' \__\/: . ./___/ \: | \  \    `.  
    '   :  | ," .--.; | |  | ' \ \'   ;   /|  ," .--.; |.   \  ' .\    ," .--.; | .  \  ' |  `----.   \ 
    ;   |.' /  /  ,.  | '  : |--' '   |  / | /  /  ,.  | \   \   ' \ |/  /  ,.  |  \  ;   : /  /`--'  / 
    '---'  ;  :   .'   \;  |,'    |   :    |;  :   .'   \ \   \  |--";  :   .'   \  \  \  ;'--'.     /  
           |  ,     .-./'--'       \   \  / |  ,     .-./  \   \ |   |  ,     .-./   :  \  \ `--'---'   
            `--`---'                `----'   `--`---'       '---"     `--`---'        \  ' ;            
                                                                                       `--`             

* It’s easy, but complex topic
* Forget that Reflections exist
* Spring (and other DI containers) are deterministic, but sometimes might work in surprising ways
  * There are hundreds of decisions and tradeoffs - knowing your library will pay off
* Coroutines are a trap! (checkout Guice)
* Don't be afraid to write useless code and re-invent the wheel as a learning opportunity
  * But never do it on production - learn to live with the constraints existing frameworks put on you









  
```

```

   ▄████████    ▄████████    ▄████████  ▄██████▄  ███    █▄     ▄████████  ▄████████    ▄████████    ▄████████ 
  ███    ███   ███    ███   ███    ███ ███    ███ ███    ███   ███    ███ ███    ███   ███    ███   ███    ███ 
  ███    ███   ███    █▀    ███    █▀  ███    ███ ███    ███   ███    ███ ███    █▀    ███    █▀    ███    █▀  
 ▄███▄▄▄▄██▀  ▄███▄▄▄       ███        ███    ███ ███    ███  ▄███▄▄▄▄██▀ ███         ▄███▄▄▄       ███        
▀▀███▀▀▀▀▀   ▀▀███▀▀▀     ▀███████████ ███    ███ ███    ███ ▀▀███▀▀▀▀▀   ███        ▀▀███▀▀▀     ▀███████████ 
▀███████████   ███    █▄           ███ ███    ███ ███    ███ ▀███████████ ███    █▄    ███    █▄           ███ 
  ███    ███   ███    ███    ▄█    ███ ███    ███ ███    ███   ███    ███ ███    ███   ███    ███    ▄█    ███ 
  ███    ███   ██████████  ▄████████▀   ▀██████▀  ████████▀    ███    ███ ████████▀    ██████████  ▄████████▀  
  ███    ███                                                   ███    ███                                      

* https://patorjk.com/software/taag - ascii art generator
* https://en.wikipedia.org/wiki/Dependency_injection
* https://github.com/google/guice/wiki/Motivation
* https://github.com/cglib/cglib/wiki
* https://www.jcp.org/en/jsr/detail?id=330
* IntCache example:
  * https://dzone.com/articles/hacking-the-integercache-in-java-9
  * https://stackoverflow.com/q/63748255
  * https://www.youtube.com/watch?v=amXXYgu0eFY









  
```

```
                                              
 _____                                    _   
|  |  | ___  _____  ___  _ _ _  ___  ___ | |_ 
|     || . ||     || -_|| | | || . ||  _|| '_|
|__|__||___||_|_|_||___||_____||___||_|  |_,_|
                                              


# Implement @Named / @Qualifier with by-name injection
* https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/annotation/Qualifier.html

# Add HTTP method arguments injection, parameter parsing etc

# Implement your own crypto service
* https://hackaday.com/2022/07/11/why-you-should-totally-roll-your-own-aes-cryptography/
* https://github.com/francisrstokes/githublog/blob/main/2022/6/15/rolling-your-own-crypto-aes.md 

# Implement ‘new instance on injection’

# Implement full JSR-330 spec











```