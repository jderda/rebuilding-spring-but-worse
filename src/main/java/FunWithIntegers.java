
public class FunWithIntegers {

	public static void main(String[] args) {
		System.out.printf("%d \n", 3 + 3);
		
        System.out.printf("%d \n", 2 + 2);
        
        System.out.printf("%d \n", new Integer(2 + 2));
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	static {
		haveFun();
	}
	private static void haveFun() {
		// won't work in JVM 9+
		// more info: https://dzone.com/articles/hacking-the-integercache-in-java-9
		try {
			Class<?> cache = Integer.class.getDeclaredClasses()[0];
			java.lang.reflect.Field c = cache.getDeclaredField("cache");
	        c.setAccessible(true);
	        Integer[] array = (Integer[]) c.get(cache);
	        array[132] = array[133];
		} catch (Exception e) {
			//do nothing
		}
	}

}

