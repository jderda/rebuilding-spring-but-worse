package com.jderda.framework;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.Set;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSet;
import com.jderda.framework.model.BeanDefinition;
import com.jderda.framework.model.BeanMatcher;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

@SuppressWarnings("unchecked")
public class MagicUtils {
	
	private final static Logger LOG = LoggerFactory.getLogger(MagicUtils.class);

	public static <T> T instantiate(BeanDefinition definition, FrameworkInjector framework) {
		Object[] params = new Object[definition.getDependencies().size()];
		for (int i = 0; i<definition.getDependencies().size(); i++) {
			params[i] = framework.getInstance(definition.getDependencies().get(i));
		}
		try {
			return (T) definition.getConstructor().newInstance(params);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public static Set<BeanMatcher> getMatchers(Class<?> klass) {
		ImmutableSet.Builder<BeanMatcher> setBuilder = ImmutableSet.builder();
		Optional<String> maybeQualifier = getQualifier(klass);
		
		for (Class<?> implInterface : klass.getInterfaces() ) {
			setBuilder.add(BeanMatcher.builder()
					.setType(implInterface)
					.setQualifier(maybeQualifier)
					.build());
		}
		
		while (!klass.equals(Object.class)) {
			setBuilder.add(BeanMatcher.builder()
					.setType(klass)
					.setQualifier(maybeQualifier)
					.build());
			klass = klass.getSuperclass();
		}
		return setBuilder.build();
	}

	public static Optional<String> getQualifier(AnnotatedElement element) {
		return Optional.ofNullable(element.getAnnotation(Named.class))
				.map(Named::value);
	}
	
	public static <T> T proxy(T instance, ProxyHandler handler, Class<? extends T> klass) {
		if (klass.getSimpleName().equals("HttpServerHandler")) {
			//we don't want to log things here
			return instance;
		}
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(klass);
		enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
		    if (method.getDeclaringClass() != Object.class) {
		        return handler.handle(method, () -> {
					try {
						return proxy.invokeSuper(obj, args);
					} catch (Throwable e) {
						throw new RuntimeException(e);
					}
				});
		    } else {
		        return proxy.invokeSuper(obj, args);
		    }
		});
		return (T) enhancer.create();
	}
	
	
}
