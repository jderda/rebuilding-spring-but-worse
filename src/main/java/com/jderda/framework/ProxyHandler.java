package com.jderda.framework;

import java.lang.reflect.Method;
import java.util.function.Supplier;

public interface ProxyHandler {
	
	public <T> T handle(Method method, Supplier<T> supplier);

}
