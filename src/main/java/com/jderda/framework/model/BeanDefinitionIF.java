package com.jderda.framework.model;

import java.lang.reflect.Constructor;
import java.util.List;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;

@Immutable
@SimpleStyle
public interface BeanDefinitionIF {
	public Constructor<?> getConstructor();
//	public List<BeanMatcher> getDependencies();
	public List<Class<?>> getDependencies();
	
	@Default
	default public boolean isRequestScoped() {
		return false;
	}

}
