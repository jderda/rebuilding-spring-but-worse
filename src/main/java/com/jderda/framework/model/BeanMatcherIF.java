package com.jderda.framework.model;

import java.util.Optional;

import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Parameter;

@Immutable
@SimpleStyle
public interface BeanMatcherIF {
	@Parameter
	public Class<?> getType();
	public Optional<String> getQualifier();
}
