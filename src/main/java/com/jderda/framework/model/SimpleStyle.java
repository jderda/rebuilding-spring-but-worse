package com.jderda.framework.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.immutables.value.Value.Style;

@Target({ ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Style(
    typeImmutable = "*",
    get = {"is*", "get*"},
    init = "set*",
    optionalAcceptNullable = true,
    typeAbstract = {"*IF", "Abstract*"})
public @interface SimpleStyle {
}

