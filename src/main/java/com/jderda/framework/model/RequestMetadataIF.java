package com.jderda.framework.model;

import org.immutables.value.Value.Immutable;

@Immutable
@SimpleStyle
public interface RequestMetadataIF {
	public long getTime();
	public String getPath();
}
