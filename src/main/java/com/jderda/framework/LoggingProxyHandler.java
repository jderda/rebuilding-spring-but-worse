package com.jderda.framework;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;

public class LoggingProxyHandler implements ProxyHandler {
	
	private final Logger log;
	
	public LoggingProxyHandler(Class<?> klass) {
		this.log = LoggerFactory.getLogger(klass);
	}

	@Override
	public <T> T handle(Method method, Supplier<T> supplier) {
		Stopwatch stopwatch = Stopwatch.createStarted();
		T result = supplier.get();
		stopwatch.stop();
		this.log.info(
				"Call to '{}#{}' finished in {} us", 
				method.getDeclaringClass().getSimpleName(), 
				method.getName(),
				stopwatch.elapsed(TimeUnit.MICROSECONDS)
		);
		return result;
	}

}
