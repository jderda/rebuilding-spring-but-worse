package com.jderda.app;

import javax.inject.Inject;

public class RandomNumbersRepositoryMock implements RandomNumbersRepository {

	
	@Inject
	public RandomNumbersRepositoryMock() {
	}
	
	public int getRandomInt() {
		return 1;
	}

	@Override
	public double getRandomDouble() {
		return 1.0;
	}

	@Override
	public String getRandomString() {
		return "mock";
	}
	
}
