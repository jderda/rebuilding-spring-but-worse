package com.jderda.app.runnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jderda.app.RandomNumbersRepositoryImpl;
import com.jderda.framework.FrameworkInjector;

public class AppInstance {
	
	static final Logger LOG = LoggerFactory.getLogger(AppInstance.class);

	public static void main(String[] args) {
		FrameworkInjector injector = new FrameworkInjector();
		
		injector.register(RandomNumbersRepositoryImpl.class);
		
		RandomNumbersRepositoryImpl repository = injector.getInstance(RandomNumbersRepositoryImpl.class);
		LOG.info("Random String: {}", repository.getRandomString());
		LOG.info("Random Int: {}", repository.getRandomInt());
		LOG.info("Random Double: {}", repository.getRandomDouble());
	}
}
