package com.jderda.app.runnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jderda.app.RequestMetadataResource;
import com.jderda.app.HttpServerHandler;
import com.jderda.app.RandomNumbersRepositoryImpl;
import com.jderda.app.RandomNumbersResource;
import com.jderda.app.RandomNumbersUseCase;
import com.jderda.framework.FrameworkInjector;

public class AppWeb {
	
	static final Logger LOG = LoggerFactory.getLogger(AppWeb.class);

	public static void main(String[] args) {
		FrameworkInjector injector = new FrameworkInjector();
		
		injector.register(HttpServerHandler.class);
		injector.register(RandomNumbersRepositoryImpl.class);
		injector.register(RandomNumbersUseCase.class);
		injector.register(RandomNumbersResource.class);
		injector.register(RequestMetadataResource.class);
	}
}
