package com.jderda.app.runnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jderda.app.RandomNumbersRepositoryImpl;
import com.jderda.app.RandomNumbersRepositoryMock;
import com.jderda.app.RandomNumbersUseCase;
import com.jderda.framework.FrameworkInjector;

public class AppBean {
	
	static final Logger LOG = LoggerFactory.getLogger(AppBean.class);

	public static void main(String[] args) {
		FrameworkInjector injector = new FrameworkInjector();
		
//		injector.register(RandomNumbersRepositoryMock.class);
		injector.register(RandomNumbersRepositoryImpl.class);
		injector.register(RandomNumbersUseCase.class);
		
		RandomNumbersUseCase useCase = injector.getInstance(RandomNumbersUseCase.class);
		LOG.info("Random String: {}", useCase.getRandomString());
		LOG.info("Random Int: {}", useCase.getRandomInt());
		LOG.info("Random Double: {}", useCase.getRandomDouble());
	}
}
