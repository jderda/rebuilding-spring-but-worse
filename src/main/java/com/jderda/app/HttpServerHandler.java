package com.jderda.app;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.jderda.framework.FrameworkInjector;
import com.jderda.framework.model.RequestMetadata;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

public class HttpServerHandler implements HttpHandler, Closeable {
	
	private final Undertow server;
	
	private Map<String, Supplier<String>> pathHandlers = new HashMap<>();
	private FrameworkInjector framework = null;
	
	@Inject
    public HttpServerHandler() {
    	this.server = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(this)
                .build();
        server.start();
    }
    
	public void registerFrameworkContext(FrameworkInjector framework) {
		this.framework = framework;
	}
    
	@Override
	public void close() throws IOException {
		this.server.stop();
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String requestPath = exchange.getRequestPath();
		if (this.framework != null) {
			RequestMetadata metadata = RequestMetadata.builder()
					.setPath(requestPath)
					.setTime(System.currentTimeMillis())
					.build();
			this.framework.registerRequestScope(RequestMetadata.class, metadata);
		}
		
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		if (pathHandlers.containsKey(requestPath)) {
			exchange.getResponseHeaders().put(Headers.STATUS, StatusCodes.OK);
			exchange.getResponseSender().send(pathHandlers.get(requestPath).get());
		} else {
	        exchange.getResponseHeaders().put(Headers.STATUS, StatusCodes.NOT_FOUND_STRING);
	        exchange.getResponseSender().send(String.format("Nope! Path '%s' not found", exchange.getRequestPath()));
		}
        
        if (this.framework != null) {
			this.framework.cleanRequestScope();
		}
	}

	public <T> void register(Class<T> klass, Supplier<T> matcher) {
		String classLevelPath = getPath(klass, "/");
		for (Method method : klass.getMethods()) {
			if (method.isAnnotationPresent(GET.class)) {
				String path = classLevelPath + getPath(method, "");
				Supplier<String> responseSupplier = () -> {
					try {
						return method.invoke(matcher.get()).toString();
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						throw new RuntimeException(e);
					}
				};
				pathHandlers.put(path, responseSupplier);
			}
		}
	}

	private <T> String getPath(AnnotatedElement element, String defaultPath) {
		return Optional.ofNullable(element.getAnnotation(Path.class))
				.map(Path::value)
				.map((path) -> (path.startsWith("/") ? path : "/" + path))
				.map((path) -> (path.endsWith("/") ? path.substring(0, path.length()-1) : path))
				.orElse(defaultPath);
	}
	
	

}
