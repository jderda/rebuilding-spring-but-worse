package com.jderda.app;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jderda.framework.RequestScoped;
import com.jderda.framework.model.RequestMetadata;

@Path("/info")
//@RequestScoped
public class RequestMetadataResource {

	private final static Logger LOG = LoggerFactory.getLogger(RequestMetadataResource.class);
	
	private final RequestMetadata request;
	
	@Inject
	public RequestMetadataResource(RequestMetadata request) {
		this.request = request;
		LOG.warn("Creating new instance of RequestMetadataResource!");
	}

	@GET
	@Path("/one")
	public RequestMetadata geRequestInfoOne() {
		return request;
	}

	@GET
	@Path("/two")
	public RequestMetadata geRequestInfoTwo() {
		return request;
	}

	@GET
	@Path("/three")
	public RequestMetadata geRequestInfoThree() {
		return request;
	}
}
