package com.jderda.app;

import javax.inject.Inject;

public class RandomNumbersUseCase {

	private RandomNumbersRepository repository;
	
//	@Inject
//	public RandomNumbersUseCase(RandomNumbersRepository repository) {
//		this.repository = repository;
//	}
	
	@Inject
	public RandomNumbersUseCase(RandomNumbersRepositoryImpl repository) {
		this.repository = repository;
	}
	
	public int getRandomInt() {
		return repository.getRandomInt();
	}
	
	public double getRandomDouble() {
		return repository.getRandomDouble();
	}
	
	public String getRandomString() {
		return repository.getRandomString();
	}
}
