package com.jderda.app;

import java.security.SecureRandom;

import javax.inject.Inject;

public class RandomNumbersRepositoryImpl implements RandomNumbersRepository {

	private SecureRandom secureRandom;
	
	@Inject
	public RandomNumbersRepositoryImpl() {
		this.secureRandom = new SecureRandom();
	}
	
	public int getRandomInt() {
		return secureRandom.nextInt();
	}

	@Override
	public double getRandomDouble() {
		return secureRandom.nextDouble();
	}

	@Override
	public String getRandomString() {
		int length = 5 + secureRandom.nextInt(12);
		StringBuffer sb = new StringBuffer();
		for (int i=0; i<length ; i++) {
			char nextChar = (char) (' ' + secureRandom.nextInt(94));
			sb.append(nextChar);
		}
		return sb.toString();
	}
	
}
