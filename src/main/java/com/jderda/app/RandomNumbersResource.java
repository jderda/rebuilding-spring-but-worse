package com.jderda.app;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/random")
public class RandomNumbersResource {

	private RandomNumbersUseCase useCase;
	
	@Inject
	public RandomNumbersResource(RandomNumbersUseCase useCase) {
		this.useCase = useCase;
	}

	@GET
	@Path("/int")
	public int getRandomInt() {
		return useCase.getRandomInt();
	}
	
	@GET
	@Path("/double")
	public double getRandomDouble() {
		return useCase.getRandomDouble();
	}
	
	@GET
	@Path("/string")
	public String getRandomString() {
		return useCase.getRandomString();
	}
}
