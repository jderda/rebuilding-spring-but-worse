package com.jderda.app;

public interface RandomNumbersRepository {

	public int getRandomInt();
	public double getRandomDouble();
	public String getRandomString();
	
}
